# MLG Trainer
Dies ist ein simples Spigot-Plugin, welches zum Trainieren von MLGs verwendet werden kann.

## Funktionsweise
Der Spieler wird, nachdem er den entsprechenden Command ausgeführt hat, in die Luft teleportiert.
Dabei findet der erste MLG aus der Höhe von 20 Blöcken statt. Bei jedem erfolgreichen MLG wird die Höhe um 10 Blöcke erhöht.

Aktuell wird nur MLG-Water unterstützt. Auf Anfrage wird gerne mehr umgesetzt.

## Commands
```
/mlg
```
Löst ein MLG-Water Training aus.

```
/mlg <typ>
```
Löst ein MLG-Training aus. Der Typ kann dabei aktuell nur "water" oder "wasser" sein,
geplant sind slime und cobweb. Sie werden auf Anfrage programmiert.

```
/mlg help
```
Hilfenachricht ähnlich wie diese Instruktion.

## Kontakt
Du hast einen Fehler/Bug gefunden oder möchtest ein neues Feature?
Melde dich gerne bei:

- Discord: ByNetherdude#8846
- E-Mail: bynetherdude@gmail.com

Alternativ kannst du auch ein Issue auf GitLab erstellen.
