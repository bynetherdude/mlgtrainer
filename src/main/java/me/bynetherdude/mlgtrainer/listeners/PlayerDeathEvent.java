package me.bynetherdude.mlgtrainer.listeners;

import me.bynetherdude.mlgtrainer.Main;
import me.bynetherdude.mlgtrainer.mlg.MLG;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class PlayerDeathEvent implements Listener {

    @EventHandler
    public void onDeath(org.bukkit.event.entity.PlayerDeathEvent e) {
        if (!Main.runningMLGs.isEmpty()) {
            if(Main.runningMLGs.containsKey((Player) e.getEntity())) {
                MLG mlg = Main.runningMLGs.get((Player) e.getEntity());
                e.setKeepInventory(true);
                e.setDeathMessage(null);
                mlg.failedMLG();
            }
        }
    }
}
