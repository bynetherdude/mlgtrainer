package me.bynetherdude.mlgtrainer;

import me.bynetherdude.mlgtrainer.listeners.EntityDamageEvent;
import me.bynetherdude.mlgtrainer.listeners.PlayerDeathEvent;
import me.bynetherdude.mlgtrainer.mlg.MLG;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import me.bynetherdude.mlgtrainer.commands.MLGCommand;

import java.util.HashMap;

public final class Main extends JavaPlugin {

    public static String prefix = "§0[§9M§fL§4G§0-§9Trainer§0]§7 ";
    public static HashMap<Player, MLG> runningMLGs = new HashMap<Player, MLG>();

    @Override
    public void onEnable() {
        // Plugin startup logic
        System.out.println("MLG-Trainer wurde gestartet!");
        commandRegistration();
        eventRegistration();
    }

    private void commandRegistration() {
        getCommand("mlg").setExecutor(new MLGCommand());
    }

    private void eventRegistration() {
        getServer().getPluginManager().registerEvents(new EntityDamageEvent(), this);
        getServer().getPluginManager().registerEvents(new PlayerDeathEvent(), this);
    }
}
