package me.bynetherdude.mlgtrainer.utils;

import me.bynetherdude.mlgtrainer.Main;
import org.bukkit.entity.Player;

public class ErrorUtils {

    public static void printInvalidArgsError(Player p, String correctArgs) {
        p.sendMessage(Main.prefix + "§c Leider war deine Eingabe nicht korrekt. Bitte verwende: §e" + correctArgs);
        p.sendMessage(Main.prefix + "Für weitere Hilfe verwende §e/mlg help");
    }
}
