package me.bynetherdude.mlgtrainer.utils;

import org.bukkit.Material;

public class GeneralUtils {

    public static boolean containsCaseInsensitive(String s, String[] l){
        for (String string : l){
            if (string.equalsIgnoreCase(s)){
                return true;
            }
        }
        return false;
    }

    public static Material getMaterialFromString(String material) {
        if(containsCaseInsensitive("Wasser", new String[]{material}) || containsCaseInsensitive("Water", new String[]{material})) {
            return Material.WATER_BUCKET;
        }
        return Material.WATER_BUCKET;
    }

    public static int calcFallSeconds(int height) {
        int duration = 2; // Default value for height below 50 blocks

        // Now set the falling duration according to the height
        if (height > 50) {
            duration = 2;
        }
        if (height > 100) {
            duration = 5;
        }
        if (height > 200) {
            duration = 9;
        }
        if (height > 300) {
            duration = 10;
        }
        if (height > 400) {
            duration = 11;
        }
        if (height > 500) {
            duration = 13;
        }
        if (height > 600) {
            duration = 14;
        }
        if (height > 700) {
            duration = 15;
        }
        if (height > 800) {
            duration = 17;
        }
        if (height > 900) {
            duration = 18;
        }

        // Return with falling duration plus time to relax (2s)
        return duration + 2;
    }
}
