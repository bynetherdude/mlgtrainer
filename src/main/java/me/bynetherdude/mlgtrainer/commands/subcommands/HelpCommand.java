package me.bynetherdude.mlgtrainer.commands.subcommands;

import me.bynetherdude.mlgtrainer.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class HelpCommand {

    public static void printHelp(Player p) {
        //help message
        p.sendMessage(Main.prefix + ChatColor.RED + "Commands:");
        p.sendMessage(Main.prefix + ChatColor.GRAY + "-------------------");
        p.sendMessage(Main.prefix + ChatColor.YELLOW + "/mlg " + ChatColor.GREEN + "Löst ein MLG-Water Training aus.");
        p.sendMessage(Main.prefix + ChatColor.YELLOW + "/mlg <typ> " + ChatColor.GREEN + "Löst ein MLG Training im angegebenen Typ aus.");
        p.sendMessage(Main.prefix + ChatColor.YELLOW + "Verfügbare Typen: " + ChatColor.GREEN + "water, wasser");
    }

}

