package me.bynetherdude.mlgtrainer.commands;

import me.bynetherdude.mlgtrainer.Main;
import me.bynetherdude.mlgtrainer.commands.subcommands.HelpCommand;
import me.bynetherdude.mlgtrainer.mlg.MLG;
import me.bynetherdude.mlgtrainer.utils.ErrorUtils;
import me.bynetherdude.mlgtrainer.utils.GeneralUtils;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MLGCommand implements CommandExecutor {

    MLG mlg;

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player p = (Player) sender;
            mlg = new MLG();
            mlg.setPlayer(p);
            mlg.setItem(Material.WATER);
            Main.runningMLGs.put(p, mlg);

            // Exit early with error if arguments are invalid
            if(!argumentsAreValid(args)) {
                ErrorUtils.printInvalidArgsError(p, "/mlg oder /mlg <typ>");
                return true;
            }

            if(args.length == 0) {
                mlg.setItem(Material.WATER_BUCKET);
                if(!mlg.isRunning()) {
                    mlg.handleMLG();
                }
            }

            if (args.length == 1) {
                // Print help message if that argument is called
                if (args[0].equalsIgnoreCase("help")) {
                    HelpCommand.printHelp(p);
                    return false;
                }

                // Call MLG function since the arguments are valid
                mlg.setItem(GeneralUtils.getMaterialFromString(args[0]));
                if(!mlg.isRunning()) {
                    mlg.handleMLG();
                }
            }
        }
        return false;
    }

    private boolean argumentsAreValid(String[] args) {
        if(args.length == 0) {
            return true;
        } else if(GeneralUtils.containsCaseInsensitive(args[0], mlg.allowedMLGTypes)) {
            return true;
        } else if(GeneralUtils.containsCaseInsensitive(args[0], new String[]{"help"})) {
            return true;
        }
        return false;
    }
}
